# blocks.love

Porting from https://simplegametutorials.github.io/love/blocks/ via tutorial to allow write new features or fix current ones when needed.

**CURRENT STEP**
- [ ] Storing inert blocks

---

<details>
<summary>DONE</summary>

- [x] Drawing the grid of blocks
    - implemented simple for loop with full support for nesting
    - enabled stripping of " from strings
- [x] Setting colors
    - fixed support for floats

</details>

<details>
<summary>NEXT STEPS</summary>

- [ ] Setting block color
- [ ] Storing the piece structures
- [ ] Storing the current piece
- [ ] Drawing the piece
- [ ] Simplifying code
- [ ] Rotation
- [ ] Testing pieces
- [ ] Setting piece position
- [ ] Moving the piece
- [ ] Timer
- [ ] Falling
- [ ] Confining movement
- [ ] Checking left of playing area
- [ ] Simplifying code
- [ ] Checking right of playing area
- [ ] Checking bottom of playing area
- [ ] Checking inert
- [ ] Simplifying code
- [ ] Drop
- [ ] Resetting piece
- [ ] Simplifying code
- [ ] Creating the sequence of next pieces
- [ ] New piece from sequence
- [ ] Add to inert
- [ ] New piece immediately after drop
- [ ] Finding complete rows
- [ ] Removing complete rows
- [ ] Game over
- [ ] Offsetting the playing area
- [ ] Drawing the upcoming piece
- [ ] Resetting the game

</details>

---

![](screenshot.png)