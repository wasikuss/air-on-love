local wrapper = require('wrapper')

function love.load()
    wrapper.load()
end

function love.draw()
    wrapper.draw()
end
