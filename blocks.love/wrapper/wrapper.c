#define LUA_LIB
#include "lua.h"

lua_State* globalL;

void Game_draw();
void Game_load();

void love_graphics_rectangle(const char* mode, int x, int y, int w, int h) {
    lua_getglobal(globalL, "love");
    lua_getfield(globalL, -1, "graphics");
    lua_getfield(globalL, -1, "rectangle");
    lua_pushlstring(globalL, mode, 5);
    lua_pushnumber(globalL, x);
    lua_pushnumber(globalL, y);
    lua_pushnumber(globalL, w);
    lua_pushnumber(globalL, h);
    lua_call(globalL, 5, 0);
}

void love_graphics_setBackgroundColor(float red, float green, float blue) {
    lua_getglobal(globalL, "love");
    lua_getfield(globalL, -1, "graphics");
    lua_getfield(globalL, -1, "setBackgroundColor");
    lua_pushnumber(globalL, red);
    lua_pushnumber(globalL, green);
    lua_pushnumber(globalL, blue);
    lua_call(globalL, 3, 0);
}

void love_graphics_setColor(float red, float green, float blue) {
    lua_getglobal(globalL, "love");
    lua_getfield(globalL, -1, "graphics");
    lua_getfield(globalL, -1, "setColor");
    lua_pushnumber(globalL, red);
    lua_pushnumber(globalL, green);
    lua_pushnumber(globalL, blue);
    lua_call(globalL, 3, 0);
}

int L_draw(lua_State* L) {
    Game_draw();
    return 0;
}

int L_load(lua_State* L) {
    Game_load();
    return 0;
}

int luaopen_wrapper(lua_State* L) {
    globalL = L;
    lua_newtable(L);
    lua_pushcfunction(L, L_draw);
    lua_setfield(L, -2, "draw");
    lua_pushcfunction(L, L_load);
    lua_setfield(L, -2, "load");
    return 1;
}
